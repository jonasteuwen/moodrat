The MoodRat.
====================

The MoodRat is software to make mood charts. Good ones.

Uses:
--------------------
  - Python
  - Flask for creating the API
  - CouchDB for data store (graphs)
  - jQuery for the graphs
  - Flask-security for login


### By:
Jonas Teuwen <jonasteuwen@gmail.com>
