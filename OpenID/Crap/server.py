#!/usr/bin/env python

# Classes? Maybe?

#__copyright__ = 'Copyright 2005-2008, Janrain, Inc.'
__copyright__ = 'Copyright 2012 Jonas Teuwen.'

from BaseHTTPServer import HTTPServer, BaseHTTPRequestHandler
from urlparse import urlparse

import time
import Cookie
import cgi
import cgitb
import sys
import bottle
import logging 

logging.basicConfig(format='localhost - - [%(asctime)s] %(message)s', level=logging.DEBUG)
log = logging.getLogger(__name__)


#Fix this fishy thing.
openid_server_name = 'openid.moodrat.nl'
openid_server_port = '80'
"""
OpenID Server and its base URL.
"""
if openid_server_port != 80:
    openid_base_url = ('http://%s:%s/' % (openid_server_name, openid_server_port))
else:
    openid_base_url = 'http://%s/' % (openid_server_name)

openid_openid = None
openid_approved = {}
openid_lastCheckIDRequest = {}



app = bottle.app()
#app = StripPathMiddleware(app)


def __init__(*args, **kwargs):
    openid_user = None
    #BaseHTTPRequestHandler.__init__(*args, **kwargs)


def quoteattr(s):
    qs = cgi.escape(s, 1)
    return '"%s"' % (qs,)

try:
    import openid
except ImportError:
    sys.stderr.write("""Failed to import the OpenID library, python-openid.""")
    sys.exit(1)

from openid.extensions import sreg
from openid.server import server
from openid.store.filestore import FileOpenIDStore
from openid.consumer import discover


class StripPathMiddleware(object):
    def __init__(app):
        self.app = app
        def __call__(e, h):
            e['PATH_INFO'] = e['PATH_INFO'].rstrip('/')
        return self.app(e, h)


@bottle.route('/')
@bottle.view('/')
def showMainPage():
    yadis_tag = '<meta http-equiv="x-xrds-location" content="%s">'%\
        (openid_base_url + 'serveryadis')
    if openid_user:
        openid_url = openid_base_url + 'id/' + openid_user
        user_message = """\
            <p>You are logged in as %s. Your OpenID identity URL is
            <tt><a href=%s>%s</a></tt>. Enter that URL at an OpenID
            consumer to test this server.</p>
            """ % (openid_user, quoteattr(openid_url), openid_url)
    else:
        user_message = """\
            <p>This server uses a cookie to remember who you are in
            order to simulate a standard Web user experience. You are
            not <a href='/login'>logged in</a>.</p>"""

    return showPage('Main Page', head_extras = yadis_tag, msg='''\
        <p>This is an OpenID server implemented using the <a
        href="http://openid.schtuff.com/">Python OpenID
        library</a>.</p>

        %s

        <p>This OpenID server needs customers which can access this server. <a href="http://moodrat.nl/">moodrat.nl</a> should be the first.</p>

        <p>The URL for this server is <a href=%s><tt>%s</tt></a>.</p>
        ''' % (user_message, quoteattr(openid_base_url), openid_base_url))

@bottle.route('/openidserver')
def openidserver():
    return serverEndPoint(self.query)


@bottle.route('/login')
def login():
    return showLoginPage('/', '/')


@bottle.route('/loginsubmit')
def dologin():
    return doLogin()

@bottle.route('/id/:userofzo')
def id():
    return showIdPage(path)

@bottle.route('/yadis/:userofzo')
def yadis():
    return showYadis(path[7:])

@bottle.route('/serveryadis')
def serveryadis():
    return showServerYadis()

@bottle.route('/allow')
def allow():
    return handleAllow(self.query)


def handleAllow(query):
    # pretend this next bit is keying off the user's session or something,
    # right?
    request = openid_lastCheckIDRequest.get(self.user)

    if 'yes' in query:
        if 'login_as' in query:
            self.user = self.query['login_as']

            if request.idSelect():
                identity = openid_base_url + 'id/' + query['identifier']
            else:
                identity = request.identity

            trust_root = request.trust_root
            if self.query.get('remember', 'no') == 'yes':
                openid_approved[(identity, trust_root)] = 'always'

            response = self.approved(request, identity)

        elif 'no' in query:
            response = request.answer(False)

        else:
            assert False, 'strange allow post.  %r' % (query,)

        self.displayResponse(response)


def setUser(self):
    cookies = self.headers.get('Cookie')
    if cookies:
        morsel = Cookie.BaseCookie(cookies).get('user')
        if morsel:
            self.user = morsel.value

def isAuthorized(identity_url, trust_root):
    if self.user is None:
        return False

    if identity_url != openid_base_url + 'id/' + self.user:
        return False

    key = (identity_url, trust_root)
    return openid_approved.get(key) is not None

def serverEndPoint(query):
    try:
        request = openid_openid.decodeRequest(query)
    except server.ProtocolError, why:
        self.displayResponse(why)
        return

    if request is None:
        # Display text indicating that this is an endpoint.
        self.showAboutPage()
        return

    if request.mode in ["checkid_immediate", "checkid_setup"]:
        self.handleCheckIDRequest(request)
    else:
        response = self.server.openid.handleRequest(request)
        self.displayResponse(response)

def addSRegResponse(request, response):
    sreg_req = sreg.SRegRequest.fromOpenIDRequest(request)

    # In a real application, this data would be user-specific,
    # and the user should be asked for permission to release
    # it.
    sreg_data = {
        'nickname':self.user
        }

    sreg_resp = sreg.SRegResponse.extractResponse(sreg_req, sreg_data)
    response.addExtension(sreg_resp)

def approved(request, identifier=None):
    response = request.answer(True, identity=identifier)
    self.addSRegResponse(request, response)
    return response

def handleCheckIDRequest(request):
    is_authorized = self.isAuthorized(request.identity, request.trust_root)
    if is_authorized:
        response = self.approved(request)
        self.displayResponse(response)
    elif request.immediate:
        response = request.answer(False)
        self.displayResponse(response)
    else:
        self.server.lastCheckIDRequest[self.user] = request
        self.showDecidePage(request)

def displayResponse(response):
    try:
        webresponse = self.server.openid.encodeResponse(response)
    except server.EncodingError, why:
        text = why.response.encodeToKVForm()
        self.showErrorPage('<pre>%s</pre>' % cgi.escape(text))
        return

    self.send_response(webresponse.code)
    for header, value in webresponse.headers.iteritems():
        self.send_header(header, value)
        self.writeUserHeader()
        self.end_headers()

        if webresponse.body:
            self.wfile.write(webresponse.body)

def doLogin(self):
    if 'submit' in self.query:
        if 'user' in self.query:
            self.user = self.query['user']
        else:
            self.user = None
            self.redirect(self.query['success_to'])
    elif 'cancel' in self.query:
        self.redirect(self.query['fail_to'])
    else:
        assert 0, 'strange login %r' % (self.query,)

def redirect(url):
    self.send_response(302)
    self.send_header('Location', url)
    self.writeUserHeader()

    self.end_headers()

def writeUserHeader(self):
    if self.user is None:
        t1970 = time.gmtime(0)
        expires = time.strftime(
            'Expires=%a, %d-%b-%y %H:%M:%S GMT', t1970)
        self.send_header('Set-Cookie', 'user=;%s' % expires)
    else:
        self.send_header('Set-Cookie', 'user=%s' % self.user)

def showAboutPage(self):
    endpoint_url = openid_base_url + 'openidserver'

    def link(url):
        url_attr = quoteattr(url)
        url_text = cgi.escape(url)
        return '<a href=%s><code>%s</code></a>' % (url_attr, url_text)

    def term(url, text):
        return '<dt>%s</dt><dd>%s</dd>' % (link(url), text)

    resources = [
        (openid_base_url, "This example server's home page"),
        ('http://www.openidenabled.com/',
         'An OpenID community Web site, home of this library'),
        ('http://www.openid.net/', 'the official OpenID Web site'),
        ]

    resource_markup = ''.join([term(url, text) for url, text in resources])

    self.showPage(200, 'This is an OpenID server', msg="""\
        <p>%s is an OpenID server endpoint.<p>
        <p>For more information about OpenID, see:</p>
        <dl>
        %s
        </dl>
        """ % (link(endpoint_url), resource_markup,))


def showDecidePage(request):
    id_url_base = openid_base_url+'id/'
    # XXX: This may break if there are any synonyms for id_url_base,
    # such as referring to it by IP address or a CNAME.
    assert (request.identity.startswith(id_url_base) or 
            request.idSelect()), repr((request.identity, id_url_base))
    expected_user = request.identity[len(id_url_base):]

    if request.idSelect(): # We are being asked to select an ID
        msg = '''\
            <p>A site has asked for your identity.  You may select an
            identifier by which you would like this site to know you.
            On a production site this would likely be a drop down list
            of pre-created accounts or have the facility to generate
            a random anonymous identifier.
            </p>
            '''
        fdata = {
            'id_url_base': id_url_base,
            'trust_root': request.trust_root,
            }
        form = '''\
            <form method="POST" action="/allow">
            <table>
              <tr><td>Identity:</td>
                 <td>%(id_url_base)s<input type='text' name='identifier'></td></tr>
              <tr><td>Trust Root:</td><td>%(trust_root)s</td></tr>
            </table>
            <p>Allow this authentication to proceed?</p>
            <input type="checkbox" id="remember" name="remember" value="yes"
                /><label for="remember">Remember this
                decision</label><br />
            <input type="submit" name="yes" value="yes" />
            <input type="submit" name="no" value="no" />
            </form>
            '''%fdata
    elif expected_user == self.user:
        msg = '''\
            <p>A new site has asked to confirm your identity.  If you
            approve, the site represented by the trust root below will
            be told that you control identity URL listed below. (If
            you are using a delegated identity, the site will take
            care of reversing the delegation on its own.)</p>'''

        fdata = {
            'identity': request.identity,
            'trust_root': request.trust_root,
            }
        form = '''\
            <table>
              <tr><td>Identity:</td><td>%(identity)s</td></tr>
              <tr><td>Trust Root:</td><td>%(trust_root)s</td></tr>
            </table>
            <p>Allow this authentication to proceed?</p>
            <form method="POST" action="/allow">
              <input type="checkbox" id="remember" name="remember" value="yes"
                  /><label for="remember">Remember this
                  decision</label><br />
              <input type="submit" name="yes" value="yes" />
              <input type="submit" name="no" value="no" />
            </form>''' % fdata
    else:
        mdata = {
            'expected_user': expected_user,
            'user': self.user,
            }
        msg = '''\
            <p>A site has asked for an identity belonging to
            %(expected_user)s, but you are logged in as %(user)s.  To
            log in as %(expected_user)s and approve the login request,
            hit OK below.  The "Remember this decision" checkbox
            applies only to the trust root decision.</p>''' % mdata

        fdata = {
            'identity': request.identity,
            'trust_root': request.trust_root,
            'expected_user': expected_user,
            }
        form = '''\
            <table>
              <tr><td>Identity:</td><td>%(identity)s</td></tr>
              <tr><td>Trust Root:</td><td>%(trust_root)s</td></tr>
            </table>
            <p>Allow this authentication to proceed?</p>
            <form method="POST" action="/allow">
              <input type="checkbox" id="remember" name="remember" value="yes"
                  /><label for="remember">Remember this
                  decision</label><br />
              <input type="hidden" name="login_as" value="%(expected_user)s"/>
              <input type="submit" name="yes" value="yes" />
              <input type="submit" name="no" value="no" />
            </form>''' % fdata

        self.showPage(200, 'Approve OpenID request?', msg=msg, form=form)

def showIdPage(path):
    link_tag = '<link rel="openid.server" href="%sopenidserver">' %\
        openid_base_url
    yadis_loc_tag = '<meta http-equiv="x-xrds-location" content="%s">'%\
        (openid_base_url+'yadis/'+path[4:])
    disco_tags = link_tag + yadis_loc_tag
    ident = openid_base_url + path[1:]

    approved_trust_roots = []
    for (aident, trust_root) in self.server.approved.keys():
        if aident == ident:
            trs = '<li><tt>%s</tt></li>\n' % cgi.escape(trust_root)
            approved_trust_roots.append(trs)

        if approved_trust_roots:
            prepend = '<p>Approved trust roots:</p>\n<ul>\n'
            approved_trust_roots.insert(0, prepend)
            approved_trust_roots.append('</ul>\n')
            msg = ''.join(approved_trust_roots)
        else:
            msg = ''

        return showPage('An Identity Page', head_extras=disco_tags, msg='''\
        <p>This is an identity page for %s.</p>
        %s
        ''' % (ident, msg))

# no Yadis yet (what the fuck is it anyway?)

def showYadis(user):
    self.send_response(200)
    self.send_header('Content-type', 'application/xrds+xml')
    self.end_headers()

    endpoint_url = openid_base_url + 'openidserver'
    user_url = openid_base_url + 'id/' + user
    self.wfile.write("""\
<?xml version="1.0" encoding="UTF-8"?>
<xrds:XRDS
    xmlns:xrds="xri://$xrds"
    xmlns="xri://$xrd*($v*2.0)">
  <XRD>

    <Service priority="0">
      <Type>%s</Type>
      <Type>%s</Type>
      <URI>%s</URI>
      <LocalID>%s</LocalID>
    </Service>

  </XRD>
</xrds:XRDS>
"""%(discover.OPENID_2_0_TYPE, discover.OPENID_1_0_TYPE,
     endpoint_url, user_url))

def showServerYadis(self):
    self.send_header('Content-type', 'application/xrds+xml')
    self.end_headers()

    endpoint_url = openid_base_url + 'openidserver'
    self.wfile.write("""\
<?xml version="1.0" encoding="UTF-8"?>
<xrds:XRDS
    xmlns:xrds="xri://$xrds"
    xmlns="xri://$xrd*($v*2.0)">
  <XRD>

    <Service priority="0">
      <Type>%s</Type>
      <URI>%s</URI>
    </Service>

  </XRD>
</xrds:XRDS>
"""%(discover.OPENID_IDP_2_0_TYPE, endpoint_url,))

def showLoginPage(success_to, fail_to):
    return showPage('Login Page', form='''\
        <h2>Login</h2>
        <p>You may log in with any name. This server does not use
        passwords because I have not implemented that yet. Haha... Yeah, rat juice.</p>
        <form method="GET" action="/loginsubmit">
          <input type="hidden" name="success_to" value="%s" />
          <input type="hidden" name="fail_to" value="%s" />
          <input type="text" name="user" value="" />
          <input type="submit" name="submit" value="Log In" />
          <input type="submit" name="cancel" value="Cancel" />
        </form>
        ''' % (success_to, fail_to))

def addbody(body, msg=None, err=None, form=None):
    if err is not None:
        body +=  '''\
            <div class="error">
              %s
            </div>
            ''' % err
    if msg is not None:
        body += '''\
            <div class="message">
              %s
            </div>
            ''' % msg

    if form is not None:
        body += '''\
            <div class="form">
              %s
            </div>
            ''' % form
    return body

def userlink(openid_user):
    if openid_user is None:
        user_link = '<a href="/login">not logged in</a>.'
    else:
        user_link = 'logged in as <a href="/id/%s">%s</a>.<br /><a href="/loginsubmit?submit=true&success_to=/login">Log out</a>' % (openid_user, openid_user)
    return user_link

def showPage(title, head_extras='', msg=None, err=None, form=None):
    user_link = userlink(openid_user)
    body = ''
    body = addbody(body)
    contents = {
        'title': 'MoodRat OpenID Server - ' + title,
        'head_extras': head_extras,
        'body': body,
        'user_link': user_link,
        }
    return contents


def main(host, port, data_path):
    addr = (host, port)

    # Instantiate OpenID consumer store and OpenID consumer.  If you
    # were connecting to a database, you would create the database
    # connection and instantiate an appropriate store here.

    store = FileOpenIDStore(data_path)
    oidserver = server.Server(store, openid_base_url + 'openidserver') #probably does not work eh lol

    httpserver.setOpenIDServer(oidserver)
    
    # Start the Bottle webapp
    bottle.debug(True)
    print 'OpenID server running at port 8000 on ' + host
    bottle.run(app=app, quiet=False, reloader=True, host='127.0.0.1', port=port, server=bottle.PasteServer) # host='127.0.0.1'


if __name__ == '__main__':
    host = 'localhost'
    data_path = 'sstore'
    port = 8000

    try:
        import optparse
    except ImportError:
        pass # Use defaults (for Python 2.2)
    else:
        parser = optparse.OptionParser('Usage:\n %prog [options]')
        parser.add_option(
            '-d', '--data-path', dest='data_path', default=data_path,
            help='Data directory for storing OpenID consumer state. '
            'Defaults to "%default" in the current directory.')
        parser.add_option(
            '-p', '--port', dest='port', type='int', default=port,
            help='Port on which to listen for HTTP requests. '
            'Defaults to port %default.')
        parser.add_option(
            '-s', '--host', dest='host', default=host,
            help='Host on which to listen for HTTP requests. '
            'Also used for generating URLs. Defaults to %default.')

        options, args = parser.parse_args()
        if args:
            parser.error('Expected no arguments. Got %r' % args)

        host = options.host
        port = options.port
        data_path = options.data_path

    main(host, port, data_path)
