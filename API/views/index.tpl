<!DOCTYPE HTML>
<html>
  <head>
    <title>%(title)s</title>
    %(head_extras)s
  </head>
  <body>
    <table class="banner">
      <tr>
        <td class="leftbanner">
          <h1><a href="/">MoodRat OpenID Server</a></h1>
        </td>
        <td class="rightbanner">
          You are %(user_link)s
        </td>
      </tr>
    </table>
    %(body)s
  </body>
</html>
