import json #has it already (couchdb)
import time
import bottle
import couchdb 
from beaker.middleware import SessionMiddleware
from cork import Cork
import logging

logging.basicConfig(format='localhost - - [%(asctime)s] %(message)s', level=logging.DEBUG)
log = logging.getLogger(__name__)

# We need to have a three step authorization. Implement soon.


@bottle.hook('before_request')
def enable_cors():
	bottle.response.headers['Access-Control-Allow-Origin'] = '*'

couch = couchdb.Server('http://127.0.0.1:5984/')
db = couch['arch']


# Use users.json and roles.json in the local example_conf directory
userdb = Cork('example_conf', email_sender='no-reply@moodrat.com', smtp_server='127.0.0.1')

import datetime
app = bottle.app()
session_opts = {
    'session.type': 'cookie',
    'session.validate_key': True,
    'session.cookie_expires': True,
    'session.timeout': 3600 * 24, # 1 day
    'session.encrypt_key': 'please use a random key and keep it secret!',
}
app = SessionMiddleware(app, session_opts)

# #  Bottle methods  # #

def postd():
    return bottle.request.forms

def post_get(name, default=''):
    return bottle.request.POST.get(name, default).strip()

@bottle.post('/login')
def login():
    """Authenticate users"""
    username = post_get('username')
    password = post_get('password')
    userdb.login(username, password, success_redirect='http://beta.moodrat.nl/old', fail_redirect='/sorry_page')

@bottle.route('/logout')
def logout():
    userdb.logout()

@bottle.post('/register')
def register():
    """Send out registration email"""
    userdb.register(post_get('username'), post_get('password'), post_get('email_address'))
    return 'Please check your mailbox.'

@bottle.route('/validate_registration/:registration_code')
def validate_registration(registration_code):
    """Validate registration, create user account"""
    userdb.validate_registration(registration_code)
    return 'Thanks. <a href="/login">Go to login</a>'

@bottle.post('/reset_password')
def send_password_reset_email():
    """Send out password reset email"""
    userdb.send_password_reset_email(
        username=post_get('username'),
        email_addr=post_get('email_address')
    )
    return 'Please check your mailbox.'

@bottle.route('/change_password/:reset_code')
@bottle.view('password_change_form')
def change_password(reset_code):
    """Show password change form"""
    return dict(reset_code=reset_code)

@bottle.post('/change_password')
def change_password():
    """Change password"""
    userdb.reset_password(post_get('reset_code'), post_get('password'))
    return 'Thanks. <a href="/login">Go to login</a>'


@bottle.route('/')
def index():
    """Only authenticated users can see this"""
    session = bottle.request.environ.get('beaker.session')
    userdb.require(fail_redirect='/login')
    return 'Welcome! <a href="/admin">Admin page</a> <a href="/logout">Logout</a>'

@bottle.route('/restricted_download')
def restricted_download():
    """Only authenticated users can download this file"""
    userdb.require(fail_redirect='/login')
    return bottle.static_file('static_file', root='.')



# Admin-only pages

@bottle.route('/admin')
@bottle.view('admin_page')
def admin():
    """Only admin users can see this"""
    userdb.require(role='admin', fail_redirect='/sorry_page')
    return dict(
        current_user = userdb.current_user,
        users = userdb.list_users(),
        roles = userdb.list_roles()
    )

@bottle.post('/create_user')
def create_user():
    try:
        userdb.create_user(postd().username, postd().role, postd().password)
        return dict(ok=True, msg='')
    except Exception, e:
        return dict(ok=False, msg=e.message)

@bottle.post('/delete_user')
def delete_user():
    try:
        userdb.delete_user(post_get('username'))
        return dict(ok=True, msg='')
    except Exception, e:
        print repr(e)
        return dict(ok=False, msg=e.message)

@bottle.post('/create_role')
def create_role():
    try:
        userdb.create_role(post_get('role'), post_get('level'))
        return dict(ok=True, msg='')
    except Exception, e:
        return dict(ok=False, msg=e.message)

@bottle.post('/delete_role')
def delete_role():
    try:
        userdb.delete_role(post_get('role'))
        return dict(ok=True, msg='')
    except Exception, e:
        return dict(ok=False, msg=e.message)

# Static pages

@bottle.route('/login')
@bottle.view('login_form')
def login_form():
    """Serve login form"""
    return {}

@bottle.route('/sorry_page')
def sorry_page():
    """Serve sorry page"""
    return '<p>Sorry, you are not authorized to perform this action</p>'


@bottle.route('/chart', method='PUT')
def put_document(): 
	data = request.body.readline()
	print data
	#print json	
	#print datajson
	if not data:
		abort(400, 'No data received') 
	entity = json.loads(data)
	if not entity.has_key('_id'):
		abort(400, 'No _id specified') 
	try: 
		db.save(entity) 
	except ValidationError as ve: 
		abort(400, str(ve)) 

@bottle.route('/chart', method='GET')
def get_document():
	data = '{"_id": "err", "name": "Go away"}'
	return data

@bottle.route('/chart/:id', method='GET') 

def get_document(id): 
	entity = db.get(id)
	if not entity:
		abort(404, 'No chart with id %s' % id)
	return entity


@bottle.route('/', method='GET')
def index():
	return "<a href='http://dev.moodrat.nl'>Go to dev.moodrat.nl</a> or check out the <a href='api/status/'>API status</a>"

@bottle.route('/api/status')
@bottle.route('/api/status/')
def api_status():
	return {'status':'online', 'servertime':time.time()}

@bottle.error(404)
def error404(error):
	return '<h1>404</h1>'


def main2():

    session_opts = {
        'session.type': 'cookie',
        'session.validate_key': True,
    }

    # Setup Beaker middleware to handle sessions and cookies
    app = bottle.default_app()
    app = SessionMiddleware(app, session_opts)

    # Start the Bottle webapp
    run(host='127.0.0.1', port=8088,server=PasteServer)


# #  Web application main  # #

def main():

    # Start the Bottle webapp
    bottle.debug(True)
    print "Test version on port 8888"
    bottle.run(app=app, quiet=False, reloader=True, host='141.138.201.187', port=8888,server=bottle.PasteServer) # host='127.0.0.1'


if __name__ == "__main__":
    main()
